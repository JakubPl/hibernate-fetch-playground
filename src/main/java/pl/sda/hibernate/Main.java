package pl.sda.hibernate;

import org.hibernate.SessionFactory;
import pl.sda.hibernate.entity.Notebook;
import pl.sda.hibernate.entity.Student;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        final SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        EntityManager entityManager = sessionFactory.createEntityManager();
        entityManager.getTransaction().begin();

        final Student student = new Student();
        student.setName("Jakub Plonka");
        List<Notebook> notebooks = Arrays.asList(new Notebook("model1", student), new Notebook("model2", student));

        notebooks.forEach(entityManager::persist);
        entityManager.persist(student);

        entityManager.getTransaction().commit();

        //TODO: eksperyment bez zamykania entity managera i ustawianiem student.setNotebooks()
        entityManager.close();
        //zamykam entity managera i otwieram nowego, bo inaczej nie strzeli nawet do bazy - skorzysta ze swojego cache

        entityManager = sessionFactory.createEntityManager();


        Student lazyStudentFromDb = entityManager.find(Student.class, student.getId());
        List<Notebook> notebooksFromDb = lazyStudentFromDb.getNotebooks();
        System.out.println("Pobieram zawartosc: ");
        notebooksFromDb.forEach(notebook -> System.out.println(notebook.getModel()));

        HibernateUtils.shutdown();


    }
}
