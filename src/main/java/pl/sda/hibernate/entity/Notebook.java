package pl.sda.hibernate.entity;

import javax.persistence.*;

@Entity
public class Notebook {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String model;

    @ManyToOne
    private Student student;

    public Notebook(String model, Student student) {
        this.model = model;
        this.student = student;
    }

    public Notebook() {
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
